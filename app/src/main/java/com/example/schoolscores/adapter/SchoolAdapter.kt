package com.example.schoolscores.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.schoolscores.R
import com.example.schoolscores.network.model.School
import com.example.schoolscores.view.DetailsActivity
import kotlinx.android.synthetic.main.rv_school_child.view.*


class SchoolAdapter (private val list: List<School>) :
    RecyclerView.Adapter<SchoolAdapter.ViewHolder>() {

    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView){
        val schoolTextView = listItemView.school_name!!
        val context = listItemView.context;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val schoolView = inflater.inflate(R.layout.rv_school_child, parent, false)

        return ViewHolder(schoolView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: SchoolAdapter.ViewHolder, position: Int) {
        val school: School = list.get(position)

        val textView = viewHolder.schoolTextView
        textView.text = school.school_name

        // Passing some info to the details screen
        textView.rootView.setOnClickListener{
            val intent = Intent(viewHolder.context, DetailsActivity::class.java)
            intent.putExtra("School_id", list[position].dbn)
            intent.putExtra("School_name", list[position].school_name)
            viewHolder.context.startActivity(intent)
        }
    }
}