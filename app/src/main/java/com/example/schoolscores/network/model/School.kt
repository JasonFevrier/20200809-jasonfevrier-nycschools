package com.example.schoolscores.network.model

import com.squareup.moshi.Json

class School (
    @field:Json(name = "id") val dbn: String,
    @field:Json(name = "school") val school_name: String
)