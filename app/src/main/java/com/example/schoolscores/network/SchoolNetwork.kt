package com.example.schoolscores.network

import com.example.schoolscores.network.model.School
import com.example.schoolscores.network.model.Score
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

const val BASE_URL = "https://data.cityofnewyork.us/resource/"

interface SchoolNetwork {

    @GET("s3k6-pzi2.json?")
    fun getSchool() : Call<List<School>>

    @GET("f9bf-2cp4.json?")
    fun getScore(@Query("dbn") score_id : String) : Call<List<Score>>

}