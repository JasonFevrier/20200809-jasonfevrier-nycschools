package com.example.schoolscores.network.model

import com.squareup.moshi.Json

class Score (
    @field:Json(name = "score_id") val dbn: String,
    @field:Json(name = "school") val school_name: String,
    @field:Json(name = "reading_score") val sat_critical_reading_avg_score: String,
    @field:Json(name = "math_score") val sat_math_avg_score: String,
    @field:Json(name = "writing_score") val sat_writing_avg_score: String

)