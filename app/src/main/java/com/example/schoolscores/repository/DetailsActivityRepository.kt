package com.example.schoolscores.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.schoolscores.network.BASE_URL
import com.example.schoolscores.network.SchoolNetwork
import com.example.schoolscores.network.model.Score
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class DetailsActivityRepository {

    val response = MutableLiveData<List<Score>>()

    fun getScore(score_id: String){
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        val retrofit = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .baseUrl(BASE_URL)
            .build()

        val service = retrofit.create(SchoolNetwork::class.java)

        /* Coroutines later */
        service.getScore(score_id).enqueue(object : Callback<List<Score>>{
            override fun onFailure(call: Call<List<Score>>, t: Throwable) {
                Log.d("DetailsRepository", "Error: $t ")
            }

            override fun onResponse(call: Call<List<Score>>, resp: Response<List<Score>>) {
                response.value = resp.body()
                Log.d("DetailsRepository", "Response: ${resp.body()} ")
            }
        })
    }
}