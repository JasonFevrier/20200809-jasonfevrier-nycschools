package com.example.schoolscores.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.schoolscores.network.BASE_URL
import com.example.schoolscores.network.SchoolNetwork
import com.example.schoolscores.network.model.School
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class SchoolActivityRepository {

    val schoolList = MutableLiveData<List<School>>()

    fun returnSchools(){
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        val retrofit = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .baseUrl(BASE_URL)
            .build()

        val service = retrofit.create(SchoolNetwork::class.java)

        /* Coroutines later */
        service.getSchool().enqueue(object : Callback<List<School>> {
            override fun onFailure(call: Call<List<School>>, t: Throwable) {
                Log.d("SchoolRepository", "Error: $t ")
            }

            override fun onResponse(call: Call<List<School>>, response: Response<List<School>>) {
                schoolList.value = response.body()
            }
        })
    }
}