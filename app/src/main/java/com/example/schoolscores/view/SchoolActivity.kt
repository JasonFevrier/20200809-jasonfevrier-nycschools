package com.example.schoolscores.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.schoolscores.R

class SchoolActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school)
    }
}