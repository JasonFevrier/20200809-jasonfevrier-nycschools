package com.example.schoolscores.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.schoolscores.R
import com.example.schoolscores.viewModel.DetailsActivityViewModel
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {
    private lateinit var viewModel : DetailsActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        viewModel = ViewModelProvider(this).get(DetailsActivityViewModel::class.java)
        if(intent.hasExtra("School_name")){
            details_school_name.text = intent.getStringExtra("School_name")
        }

        if(intent.hasExtra("School_id")){
            //Network
            if(intent.getStringExtra("School_id") != null)
                viewModel.getScore(intent.getStringExtra("School_id")!!)
        }

        viewModel.response.observe(this, Observer {
           //Could use databinding instead
            if(!it.isNullOrEmpty()) {
                details_reading_score.text = it[0].sat_critical_reading_avg_score
                details_math_score.text = it[0].sat_math_avg_score
                details_writing_score.text = it[0].sat_writing_avg_score
            }
        })
    }

}