package com.example.schoolscores.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.schoolscores.R
import com.example.schoolscores.adapter.SchoolAdapter
import com.example.schoolscores.viewModel.SchoolActivityViewModel
import kotlinx.android.synthetic.main.activity_school.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel : SchoolActivityViewModel
    private lateinit var adapter: SchoolAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school)

        viewModel = ViewModelProvider(this).get(SchoolActivityViewModel::class.java)

        viewModel.returnSchools()

        viewModel.schoolList.observe(this, Observer{
            adapter = SchoolAdapter(it)
            rv_school.adapter = adapter
        })
    }
}