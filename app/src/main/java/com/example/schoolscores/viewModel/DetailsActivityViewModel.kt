package com.example.schoolscores.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.schoolscores.network.model.Score
import com.example.schoolscores.repository.DetailsActivityRepository


class DetailsActivityViewModel : ViewModel() {

    private val repository = DetailsActivityRepository()

    val response : LiveData<List<Score>>

    init {
        response = repository.response
    }

    fun getScore(score_id : String){
        repository.getScore(score_id)
    }

}