package com.example.schoolscores.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.schoolscores.network.model.School
import com.example.schoolscores.repository.SchoolActivityRepository

class SchoolActivityViewModel : ViewModel() {

    private val repository = SchoolActivityRepository()

    val schoolList : LiveData<List<School>>

    init {
        this.schoolList = repository.schoolList
    }

    fun returnSchools(){
        repository.returnSchools()
    }
}