# 20200809-JasonFevrier-NYCSchools

NYCSchools project implemented using Retrofit2, Moshi and kotlin.

Using a RecyclerView + Adapter for the list of schools, implemented focusing on proper separation of concerns.

With some more time, would possible add coroutines and a search function for the school list

